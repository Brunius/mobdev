package me.brunius.assignment41;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Parcelable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ToggleButton;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MainActivity extends AppCompatActivity {
	public static final String INTENT_MESSAGE = "me.brunius.assignment41.FORM";
	public static final String LOG_PREFIX = "BRUNIUS_";
	public static final int FORM_REQUEST = 1;

	formData crepeData;
	formData baconData;
	formData appleData;
	formData sorbetData;


	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		if (savedInstanceState != null) {
			crepeData = savedInstanceState.getParcelable("crepes");
			baconData = savedInstanceState.getParcelable("baconeggs");
			appleData = savedInstanceState.getParcelable("applepie");
			sorbetData = savedInstanceState.getParcelable("sorbet");
			Log.v(LOG_PREFIX + "ONCREATE", "Recovering saved state from memory...");
		}
		if (crepeData == null) {
			crepeData = new formData();
			Log.v(LOG_PREFIX + "ONCREATE", "Creating new crepeData...");
		}
		if (baconData == null) {
			baconData = new formData();
			Log.v(LOG_PREFIX + "ONCREATE", "Creating new baconData...");
		}
		if (appleData == null) {
			appleData = new formData();
			Log.v(LOG_PREFIX + "ONCREATE", "Creating new appleData...");
		}
		if (sorbetData == null) {
			sorbetData = new formData();
			Log.v(LOG_PREFIX + "ONCREATE", "Creating new sorbetData...");
		}

		//Load pictures into buttons
		ImageButton button_crepes = (ImageButton) findViewById(R.id.button_crepes);
		button_crepes.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.crepes));

		ImageButton button_baconeggs = (ImageButton) findViewById(R.id.button_baconeggs);
		button_baconeggs.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.bacon_eggs));

		ImageButton button_applepie = (ImageButton) findViewById(R.id.button_applepie);
		button_applepie.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.apple_pie));

		ImageButton button_sorbet = (ImageButton) findViewById(R.id.button_sorbet);
		button_sorbet.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.sorbet));

		Log.v(LOG_PREFIX + "ONCREATE", "Finished assigning images.");
	}

	public void openForm(View view) {
		Pattern pattern = Pattern.compile("app:id\\/button_(.+)\\}");
		Matcher matcher = pattern.matcher(view.toString());
		matcher.find();
		String tempString = matcher.group(1);

		//Setup the intent and stuff
		Intent formIntent = new Intent(this, formActivity.class);
		Log.v(LOG_PREFIX + "INTENT_SEND", "Created intent (M>F)");

		//Pass it the data about which image it's referencing.
		Log.v(LOG_PREFIX + "INTENT_SEND", "tempString:  " + tempString);
		formIntent.putExtra(INTENT_MESSAGE, tempString);
		formData passThisData;
		switch (tempString) {
			case "crepes":
				passThisData = crepeData;
				break;
			case "applepie":
				passThisData = appleData;
				break;
			case "baconeggs":
				passThisData = baconData;
				break;
			case "sorbet":
				passThisData = sorbetData;
				break;
			default:
				passThisData = new formData();
				Log.v(LOG_PREFIX + "INTENT_SEND", "Something has gone very wrong.");
		}
		formIntent.putExtra(tempString, passThisData);
		startActivityForResult(formIntent, FORM_REQUEST);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (data == null) {
			Log.v("BRUNIUS_TEMP", "data is null! abort! abort!");
		}
		// Check which request we're responding to
		if (requestCode == FORM_REQUEST) {
			// Make sure the request was successful
			if (resultCode == RESULT_OK) {
				//Check the intent for the formdata
				formData newFormData = data.getParcelableExtra(data.getStringExtra(INTENT_MESSAGE));
				switch (data.getStringExtra(INTENT_MESSAGE)) {
					case "crepes":
						crepeData = newFormData;
						break;
					case "applepie":
						appleData = newFormData;
						break;
					case "baconeggs":
						baconData = newFormData;
						break;
					case "sorbet":
						sorbetData = newFormData;
						break;
					default:
						Log.v(LOG_PREFIX + "ACTIVITY_RESULT", "Something has gone very wrong.");
				}
			}
		} else {
			Log.v(LOG_PREFIX + "ACTIVITY_RESULT", "Invalid request code received.");
		}
	}

	//This ensures our data is kept in between screen rotations etc.
	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		outState.putParcelable("crepes", crepeData);
		outState.putParcelable("baconeggs", baconData);
		outState.putParcelable("applepie", appleData);
		outState.putParcelable("sorbet", sorbetData);
	}
}
