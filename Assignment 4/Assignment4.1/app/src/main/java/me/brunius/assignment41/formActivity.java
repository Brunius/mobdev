package me.brunius.assignment41;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Parcelable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.ToggleButton;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class formActivity extends AppCompatActivity {
	formData myFormData;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_form);

		//Get the intent
		Intent intent = getIntent();

		//From the intent, retrieve the reference to image
		String imageString = intent.getStringExtra(MainActivity.INTENT_MESSAGE);
		Log.v(MainActivity.LOG_PREFIX + "INTENT_RECEIVE", "Found intent with message: " + imageString);

		if (savedInstanceState == null) {
			Log.v(MainActivity.LOG_PREFIX + "INTENT_RECEIVE", "No saved instance state found, grabbing it from the intent");
			myFormData = intent.getParcelableExtra(imageString);
		} else {
			//Decide which image the string correlates with
			switch (imageString) {
				case "crepes":
				case "applepie":
				case "baconeggs":
				case "sorbet":
					//All four of these options point to this location, as it should be getting the parcelable with the same method either way.
					Log.v(MainActivity.LOG_PREFIX + "INTENT_RECEIVE", "Found saved instance state, retrieving form data...");
					myFormData = savedInstanceState.getParcelable(imageString);
					break;
				default:
					Log.v(MainActivity.LOG_PREFIX + "INTENT_RECEIVE", "No compatible string located for " + imageString);
			}
		}

		EditText nameObject = (EditText) findViewById(R.id.edit_name);
		EditText urlObject = (EditText) findViewById(R.id.edit_url);
		EditText keywordObject = (EditText) findViewById(R.id.edit_keywords);
		EditText dateObject = (EditText) findViewById(R.id.edit_date);
		ToggleButton shareObject = (ToggleButton) findViewById(R.id.edit_share);
		EditText obtainObject = (EditText) findViewById(R.id.edit_email);
		EditText ratingObject = (EditText) findViewById(R.id.edit_rating);

		nameObject.setText(myFormData.Name());
		urlObject.setText(myFormData.Location());
		keywordObject.setText(myFormData.Keyword());
		dateObject.setText(myFormData.DateObtained());
		shareObject.setChecked(myFormData.Share());
		obtainObject.setText(myFormData.ObtainedBy());
		ratingObject.setText(String.valueOf(myFormData.Rating()));
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		EditText nameObject = (EditText) findViewById(R.id.edit_name);
		EditText urlObject = (EditText) findViewById(R.id.edit_url);
		EditText keywordObject = (EditText) findViewById(R.id.edit_keywords);
		EditText dateObject = (EditText) findViewById(R.id.edit_date);
		ToggleButton shareObject = (ToggleButton) findViewById(R.id.edit_share);
		EditText obtainObject = (EditText) findViewById(R.id.edit_email);
		EditText ratingObject = (EditText) findViewById(R.id.edit_rating);

		myFormData.Name(nameObject.getText().toString());
		myFormData.Location(urlObject.getText().toString());
		myFormData.Keyword(keywordObject.getText().toString());
		myFormData.DateObtained(dateObject.getText().toString());
		myFormData.Share(shareObject.isChecked());
		myFormData.ObtainedBy(obtainObject.getText().toString());
		//If the rating box is empty, and we pass it an empty string, it crashes. So we only pass it the contents of the rating box if it's not empty
		if (!ratingObject.getText().toString().trim().isEmpty()) {
			myFormData.Rating(Integer.parseInt(ratingObject.getText().toString().trim()));
		}

		outState.putParcelable(getIntent().getStringExtra(MainActivity.INTENT_MESSAGE), myFormData);
	}

	public void submitForm(View view) {
		EditText nameObject = (EditText) findViewById(R.id.edit_name);
		EditText urlObject = (EditText) findViewById(R.id.edit_url);
		EditText keywordObject = (EditText) findViewById(R.id.edit_keywords);
		EditText dateObject = (EditText) findViewById(R.id.edit_date);
		ToggleButton shareObject = (ToggleButton) findViewById(R.id.edit_share);
		EditText obtainObject = (EditText) findViewById(R.id.edit_email);
		EditText ratingObject = (EditText) findViewById(R.id.edit_rating);
		TextView errorObject = (TextView) findViewById(R.id.label_errors);

		String error_string = "";

		//This will be called when the submit button is pressed
		if (nameObject.getText().toString().trim().isEmpty()) {
			//Throw your error for it being empty
			Log.v(MainActivity.LOG_PREFIX + "FORM_SUBMISSION", "Name is empty");
			error_string = error_string + "Name is empty\n";
		}
		if (obtainObject.getText().toString().trim().isEmpty()) {
			//Throw your error for it being empty
			Log.v(MainActivity.LOG_PREFIX + "FORM_SUBMISSION", "Email is empty");
			error_string = error_string + "Email is empty\n";
		} else {
			//Using regex to match the email
			Pattern pattern = Pattern.compile(".+@.+\\..+");//This is a really rough regex for checking email. It probably lets through a lot of bogus emails.
			Matcher matcher = pattern.matcher(obtainObject.getText().toString());
			if (!matcher.find()) {
				//Throw your error for it not being an email
				Log.v(MainActivity.LOG_PREFIX + "FORM_SUBMISSION", "Email is not an email");
				error_string = error_string + "Please check your email is correct\n";
			}
		}
		//If the rating box is empty, and we pass it an empty string, it crashes. So we only pass it the contents of the rating box if it's not empty
		if (!ratingObject.getText().toString().trim().isEmpty()) {
			if (Integer.parseInt(ratingObject.getText().toString()) < 0 || Integer.parseInt(ratingObject.getText().toString()) > 5) {
				//Throw your error for it not being within the required range.
				Log.v(MainActivity.LOG_PREFIX + "FORM_SUBMISSION", "Integer is out of range");
				error_string = error_string + "Please make sure your rating is between 0 and 5 (inclusive)\n";
			}
		} else {
			error_string = error_string + "Please make sure your rating is between 0 and 5 (inclusive)\n";
		}
		//If the error string is empty, no problems have been detected, so we can save the data.
		if (error_string.trim().isEmpty()) {
			Log.v(MainActivity.LOG_PREFIX + "FORM_SUBMISSION", "Error String: " + error_string);
			Log.v(MainActivity.LOG_PREFIX + "FORM_SUBMISSION", "Submitting data...");
			myFormData.Name(nameObject.getText().toString().trim());
			myFormData.Location(urlObject.getText().toString().trim());
			myFormData.Keyword(keywordObject.getText().toString().trim());
			myFormData.DateObtained(dateObject.getText().toString().trim());
			myFormData.Share(shareObject.isChecked());
			myFormData.ObtainedBy(obtainObject.getText().toString().trim());
			myFormData.Rating(Integer.parseInt(ratingObject.getText().toString().trim()));
			errorObject.setText("");
			Intent mainIntent = getIntent();
			mainIntent.putExtra(MainActivity.INTENT_MESSAGE, mainIntent.getStringExtra(MainActivity.INTENT_MESSAGE));
			mainIntent.putExtra(mainIntent.getStringExtra(MainActivity.INTENT_MESSAGE), myFormData);
			setResult(RESULT_OK, mainIntent);
			Log.v(MainActivity.LOG_PREFIX + "FORM_SUBMISSION", "Finished submitting data");
			finish();
		} else {
			Log.v(MainActivity.LOG_PREFIX + "FORM_SUBMISSION", "Error String: " + error_string);
			Log.v(MainActivity.LOG_PREFIX + "FORM_SUBMISSION", "Popping up error...");
			errorObject.setText(error_string);
			Log.v(MainActivity.LOG_PREFIX + "FORM_SUBMISSION", "Finished popping up error");
		}
	}
}
