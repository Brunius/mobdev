package me.brunius.assignment41;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

/**
 * Created by Ben Grunow on 2017-09-14.
 */

public class formData implements Parcelable {
	String name;
	String url;
	String keywords;
	String dateObtained;
	Boolean share;
	String email;
	int rating;

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel out, int flags) {
		out.writeString(name);
		out.writeString(url);
		out.writeString(keywords);
		out.writeString(dateObtained);
		out.writeValue(share);
		out.writeString(email);
		out.writeInt(rating);
	}

	public static final Parcelable.Creator<formData> CREATOR = new Parcelable.Creator<formData>() {
		public formData createFromParcel(Parcel in) {
			return new formData(in);
		}

		public formData[] newArray(int size) {
			return new formData[size];
		}
	};

	public formData() {
		name = "";
		url = "";
		keywords = "";
		dateObtained = new Date().toLocaleString().substring(0,11);
		share = false;
		email = "";
		rating = 0;
	}

	public formData(Parcel in) {
		name = in.readString();
		url = in.readString();
		keywords = in.readString();
		dateObtained = in.readString();
		share = (Boolean) in.readValue(boolean.class.getClassLoader());	//WHY IS THIS SO COMPLICATED
																		//I JUST WANTED A BOOLEAN
																		//WHY IS THAT SO HARD
		email = in.readString();
		rating = in.readInt();
	}

	//----------------------------------GETTERS & SETTERS----------------------------------

	public String Name () {
		return name;
	}

	public void Name (String newData) {
		name = newData;
	}

	public String Location () {
		return url;
	}

	public void Location (String newData) {
		url = newData;
	}

	public String Keyword () {
		return keywords;
	}

	public void Keyword (String newData) {
		keywords = newData;
	}

	public String DateObtained() {
		return dateObtained;
	}

	public void DateObtained (String newData) {
		dateObtained = newData;
	}

	public Boolean Share () {
		return share;
	}

	public void Share (Boolean newData) {
		share = newData;
	}

	public String ObtainedBy () {
		return email;
	}

	public void ObtainedBy (String newData) {
		email = newData;
	}

	public int Rating () {
		return rating;
	}

	public void Rating (int newData) {
		//Only pass ratings between 0 and 5.
		if (newData >= 0 && newData <= 5) {
			rating = newData;
		}
	}

	//--------------------------------END GETTERS & SETTERS--------------------------------
}
