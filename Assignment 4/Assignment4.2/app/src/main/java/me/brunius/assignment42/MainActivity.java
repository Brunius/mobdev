package me.brunius.assignment42;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {

	private float text_size = 32;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		TextView name_label = (TextView) findViewById(R.id.label_name);
		TextView url_label = (TextView) findViewById(R.id.label_url);
		TextView keyword_label = (TextView) findViewById(R.id.label_keywords);
		TextView date_label = (TextView) findViewById(R.id.label_date);
		TextView share_label = (TextView) findViewById(R.id.label_share);
		TextView obtained_label = (TextView) findViewById(R.id.label_email);
		TextView rating_label = (TextView) findViewById(R.id.label_rating);

		name_label.setTextSize(text_size);
		url_label.setTextSize(text_size);
		keyword_label.setTextSize(text_size);
		date_label.setTextSize(text_size);
		share_label.setTextSize(text_size);
		obtained_label.setTextSize(text_size);
		rating_label.setTextSize(text_size);
	}

	public void submitForm(View view) {
		Intent newIntent = new Intent(this, formActivity.class);

		Log.v("BRUNIUS_USABILITY", "MAIN TO FORM");

		startActivity(newIntent);
	}
}