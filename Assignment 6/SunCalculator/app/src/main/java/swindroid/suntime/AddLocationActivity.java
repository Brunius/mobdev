package swindroid.suntime;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

import java.text.NumberFormat;

import swindroid.suntime.ui.Main;

public class AddLocationActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_location);
	}

	public void finishedButtonHandler(View v) {
		EditText nameText = (EditText) findViewById(R.id.name_edit);
		EditText latiText = (EditText) findViewById(R.id.latitude_edit);
		EditText longText = (EditText) findViewById(R.id.longitude_edit);
		EditText timeText = (EditText) findViewById(R.id.timezone_edit);

		String errorText = "";
		if (nameText.getText().toString().isEmpty()) {
			errorText += "No name specified\n";
		}
		if (latiText.getText().toString().isEmpty()) {
			errorText += "No latitude specified\n";
		} else {
			try {
				Double.parseDouble(latiText.getText().toString());
			} catch (NumberFormatException exception) {
				errorText += "Latitude in wrong format\n";
			}
		}
		if (longText.getText().toString().isEmpty()) {
			errorText += "No longitude specified\n";
		} else {
			try {
				Double.parseDouble(longText.getText().toString());
			} catch (NumberFormatException exception) {
				errorText += "Longitude in wrong format\n";
			}
		}
		if (timeText.getText().toString().isEmpty()) {
			errorText += "No timezone specified\n";
		} else {
			try {
				int timezone = Integer.parseInt(timeText.getText().toString());
				if ((timezone < -14) || (timezone > 12)) {
					errorText += "Timezone is out of range\n";
				}
			} catch (NumberFormatException exception) {
				errorText += "Timezone in wrong format\n";
			}
		}
		if (errorText.trim().isEmpty()) {
			Log.v("BRUNIUS_ADDLOC_RETURN", "Returning list with key \"" + nameText.getText() + "\"");
			Intent mainIntent = getIntent();

			Location myLocation = new Location("USER_ENTERED");
			myLocation.setLatitude(Double.parseDouble(latiText.getText().toString()));
			myLocation.setLongitude(Double.parseDouble(longText.getText().toString()));
			Log.v("BRUNIUS_TEMP_ADD", "Lat: " + myLocation.getLatitude() + "\nLon: " + myLocation.getLongitude());

			String timezonePrefix = "Etc/GMT";
			if (Integer.parseInt(timeText.getText().toString()) > 0) {
				timezonePrefix += "+";
			}

			//Put all the data in here - name, latitude/longitude, timezone offset
			mainIntent.putExtra(Main.NAME_KEY, nameText.getText().toString());
			mainIntent.putExtra(Main.LOCATION_KEY, myLocation);
			mainIntent.putExtra(Main.TIMEZONE_KEY, timezonePrefix + Integer.parseInt(timeText.getText().toString()));
			Log.v("BRUNIUS_TEMP", "Timezone: " + mainIntent.getStringExtra(Main.TIMEZONE_KEY));
			setResult(RESULT_OK, mainIntent);
			finish();
		} else {
			Log.v("BRUNIUS_ADDLOC_RETURN", errorText);
		}
	}
}
