package me.brunius.assignment62;

import android.app.ListActivity;
import android.location.Location;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class MainActivity extends ListActivity {

	private HashMap<String, Integer> bookRating = new HashMap<>();
	private HashMap<String, Integer> bookImage = new HashMap<>();

	private String[] getBookNames() {
		List<String> outputData = new ArrayList<>();
		for (String item : bookRating.keySet()) {
			outputData.add(item);
		}

		Collections.sort(outputData);

		return outputData.toArray(new String[outputData.size()]);
	}

	private void loadBookData() {
		InputStream file = getResources().openRawResource(R.raw.booklist);
		Scanner inputStream = new Scanner(file);

		inputStream.useDelimiter("[,\n]");
		while (inputStream.hasNext()){
			String tempString = inputStream.next();
			Log.v("BRUNIUS_HASHMAP", "Got " + tempString);
			Integer tempInteger = inputStream.nextInt();
			Log.v("BRUNIUS_HASHMAP", "Got " + tempInteger);
			Integer tempImageRef = inputStream.nextInt();
			Log.v("BRUNIUS_HASHMAP", "Got " + tempImageRef);
			bookRating.put(tempString, tempInteger);
			bookImage.put(tempString, tempImageRef);
		}

	}

	private void initialiseUI() {
		String[] bookList = getBookNames();

		ArrayAdapter<String> adapter = new RowIconAdapter();
		setListAdapter(adapter);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		loadBookData();
		initialiseUI();
	}

	class RowIconAdapter extends ArrayAdapter<String> {
		public RowIconAdapter() {
			super(MainActivity.this, R.layout.book_list, R.id.row_label, getBookNames());
		}

		public View getView(int pos, View cView, ViewGroup parent) {
			View row = super.getView(pos, cView, parent);
			// get current image
			ImageView icon = (ImageView) row.findViewById(R.id.row_icon);
			TextView rating = (TextView) row.findViewById(R.id.row_rating);
getit
			String book = getBookNames()[pos];

			int image = R.drawable.ic_book_black_24dp;

			switch (bookImage.get(book)) {
				case 1:
					//The Hunger Games
					image = R.drawable.ic_restaurant_black_24dp;
					break;
				case 2:
					//Harry Potter
					image = R.drawable.ic_lightbulb_outline_black_24dp;
					break;
				case 3:
					//To Kill a Mockingbird
					image = R.drawable.ic_account_balance_black_24dp;
					break;
				case 4:
					//Pride and Prejudice
					image = R.drawable.ic_phone_android_black_24dp;
					break;
				case 5:
					//Twilight
					image = R.drawable.ic_wb_sunny_black_24dp;
					break;
				default:
			}

			icon.setImageResource(image);
			rating.setText("Rating: " + bookRating.get(book).toString());

			return row;
		}
	}
}
