package swindroid.suntime;

import android.app.ListActivity;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import java.util.TimeZone;

import swindroid.suntime.ui.Main;

public class LatLongActivity extends ListActivity {
	//TODO: Use timezone data
	private HashMap<String, Location> locations = new HashMap<>();
	private HashMap<String, String> timezones = new HashMap<>();

	public void populateHashMap() {
		InputStream file = getResources().openRawResource(R.raw.au_locations);
		Scanner inputStream = new Scanner(file);

		inputStream.useDelimiter("[,\n]");
		while (inputStream.hasNext()){
			String tempString = inputStream.next();
			Location tempLocation = new Location("FROM_FILE");
			tempLocation.setLatitude(inputStream.nextDouble());
			tempLocation.setLongitude(inputStream.nextDouble());
			String tempTimezone = inputStream.next();
			locations.put(tempString, tempLocation);
			timezones.put(tempString, tempTimezone);
			Log.v("BRUNIUS_HASHMAP", "Put " + tempString + " into hashmap");
		}
	}

	private String[] getCityNames() {
		List<String> outputData = new ArrayList<>();
		for (String item : locations.keySet()) {
			outputData.add(item);
		}

		Collections.sort(outputData);

		return outputData.toArray(new String[outputData.size()]);
	}

	private void initialiseUI() {
		String[] cities = getCityNames();

		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, cities);
		setListAdapter(adapter);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_list);

		populateHashMap();
		initialiseUI();
	}

	@Override
	public void onListItemClick(ListView listview, View view, int position, long id) {
		returnList(getCityNames()[position]);
	}

	public void returnList(String key) {
		Log.v("BRUNIUS_LATLONG_RETURN", "Returning list with key \"" + key + "\"");
		Intent mainIntent = getIntent();
		//Put all the data in here - name, latitude/longitude, timezone offset
		mainIntent.putExtra(Main.NAME_KEY, key);
		mainIntent.putExtra(Main.LOCATION_KEY, locations.get(key));
		mainIntent.putExtra(Main.TIMEZONE_KEY, timezones.get(key));
		setResult(RESULT_OK, mainIntent);
		finish();
	}
}
