package swindroid.suntime.ui;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import swindroid.suntime.LatLongActivity;
import swindroid.suntime.R;
import swindroid.suntime.calc.AstronomicalCalendar;
import swindroid.suntime.calc.GeoLocation;
import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.DatePicker.OnDateChangedListener;
import android.widget.TextView;

public class Main extends Activity 
{
	public static final int NEW_LOCATION = 204;

	public static final String NAME_KEY = "NAME KEY";
	public static final String LOCATION_KEY = "LATLON KEY";
	public static final String TIMEZONE_KEY = "TIMEZONE KEY";

	private String name = "Melbourne";
	private Location loc = new Location("DEFAULT");
	TimeZone tz = TimeZone.getDefault();

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
		loc.setLatitude(-37.50);
		loc.setLongitude(145.01);
        initializeUI();
	}

	private void initializeUI()
	{
		DatePicker dp = (DatePicker) findViewById(R.id.datePicker);
		Calendar cal = Calendar.getInstance();
		int year = cal.get(Calendar.YEAR);
		int month = cal.get(Calendar.MONTH);
		int day = cal.get(Calendar.DAY_OF_MONTH);
		dp.init(year,month,day,dateChangeHandler); // setup initial values and reg. handler
		updateTime(year, month, day);
	}

	private void updateTime(int year, int monthOfYear, int dayOfMonth)
	{
		GeoLocation geolocation = new GeoLocation(name, loc.getLatitude(), loc.getLongitude(), tz);
		AstronomicalCalendar ac = new AstronomicalCalendar(geolocation);
		ac.getCalendar().set(year, monthOfYear, dayOfMonth);
		Date srise = ac.getSunrise();
		Date sset = ac.getSunset();
		
		SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
		
		TextView sunriseTV = (TextView) findViewById(R.id.sunriseTimeTV);
		TextView sunsetTV = (TextView) findViewById(R.id.sunsetTimeTV);
		Log.d("SUNRISE Unformatted", srise+"");
		
		sunriseTV.setText(sdf.format(srise));
		sunsetTV.setText(sdf.format(sset));		
	}
	
	OnDateChangedListener dateChangeHandler = new OnDateChangedListener()
	{
		public void onDateChanged(DatePicker dp, int year, int monthOfYear, int dayOfMonth)
		{
			updateTime(year, monthOfYear, dayOfMonth);
		}	
	};


	public void onLocationClick (View v) {
		Intent getNewLocation = new Intent(this, LatLongActivity.class);
		startActivityForResult(getNewLocation, NEW_LOCATION);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (data == null) {
			Log.v("BRUNIUS_TEMP", "Data is null - something has gone terrible wrong");
		}
		if (requestCode == NEW_LOCATION) {
			if (resultCode == RESULT_OK) {
				TextView locationTV = (TextView) findViewById(R.id.locationTV);
				locationTV.setText(data.getStringExtra(NAME_KEY) + ", AU");
				name = data.getStringExtra(NAME_KEY);
				loc = data.getParcelableExtra(LOCATION_KEY);
				tz = TimeZone.getTimeZone(data.getStringExtra(TIMEZONE_KEY));
				initializeUI();
			} else {
				Log.v("BRUNIUS_ACTIVITY_RESULT", "Invalid result code received");
			}
		} else {
			Log.v("BRUNIUS_ACTIVITY_RESULT", "Invalid request code received");
		}
	}
}