package me.brunius.assignment31;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Locale;

/**
 * Concepts that are introduced in this example are:
 * 1. Handling button clicks (Listener, Anonymous methods, Registering)
 * 2. Reading text from an input field and setting the text.
 * 3. Controlling the keyboard that is displayed for an input field
 * 4. Use of the bitmask based option setting for input field keyboard control
 * 5. Layout management -- fill_parent (layouts are heirarchial)
 * 
 * Knowledge assumed to understand this example:
 * 1. FindViewById
 * 2. Use of XML to create layouts
 *  
 * @author Rajesh Vasa
 */
public class TemperatureConvertor extends AppCompatActivity
{

	TextView convertedText;
	EditText inputTempText;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_temperature);

        initializeUI();
    }

	private void initializeUI()
	{
		Button convertButton = (Button)findViewById(R.id.convertButton);
	    convertButton.setOnClickListener(convertBtnListener);
	    convertButtonClicked();
	}
    
	/** Handle convert button click */ 
	private OnClickListener convertBtnListener = new OnClickListener() 
	{
	    public void onClick(View v) 
	    {
  	    		convertButtonClicked();
	    }
	};

	private void convertButtonClicked()
	{
		inputTempText = (EditText) findViewById(R.id.inputTempEditText);
		convertedText = (TextView) findViewById(R.id.convertedTempTextView);
		String frStr = convertToFh(inputTempText.getText().toString());
		convertedText.setText(frStr+" F");
	}
	
	/** Convert celcius provided to farenheit 
	 * 
	// * @param celcius A valid float number
	 * @return Fahrenheit numeric value or "ERR" if unable to
	 */
	private String convertToFh(String pCelcius)
	{
		try
		{
			double c = Double.parseDouble(pCelcius);
			double f = c * (9.0/5.0) + 32.0;
			return String.format(Locale.getDefault(), "%3.2f", f);
		}
		catch (NumberFormatException nfe)
		{
			return "ERR";
		}
	}

	//Restores our data, when it's lost
	@Override
	public void onRestoreInstanceState(Bundle savedInstanceState) {
		//Note that text_converted is located and set in onCreate
		//If onCreate is called after this function, it will crash, with a null pointer exception
		convertedText.setText(savedInstanceState.getString("CONVERTED_VALUE"));
		inputTempText.setText(savedInstanceState.getString("UNCONVERTED_VALUE"));
	}

	//Saves our data, so it's not lost
	@Override
	public void onSaveInstanceState(Bundle outState) {
		outState.putString("CONVERTED_VALUE", convertedText.getText().toString());
		outState.putString("UNCONVERTED_VALUE", inputTempText.getText().toString());
		super.onSaveInstanceState(outState);
	}
}