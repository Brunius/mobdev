package me.brunius.assignment31;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void goToDistance(View view) {
        Intent distanceIntent = new Intent(this, distanceActivity.class);
        Log.v("BRUNIUS_SWITCH_INTENT", "Intent created, about to pass to startActivity...");
        startActivity(distanceIntent);
        Log.v("BRUNIUS_SWITCH_INTENT", "Passed distanceIntent to startActivity");
    }

    public void goToTemperature(View view) {
        Intent temperatureIntent = new Intent(this, TemperatureConvertor.class);
        Log.v("BRUNIUS_SWITCH_INTENT","Intent created, about to pass to startActivity...");
        startActivity(temperatureIntent);
        Log.v("BRUNIUS_SWITCH_INTENT", "Passed temperatureIntent to startActivity");
    }
}
