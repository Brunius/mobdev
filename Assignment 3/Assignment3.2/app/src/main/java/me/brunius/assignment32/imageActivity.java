package me.brunius.assignment32;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class imageActivity extends AppCompatActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_image);

		//Get the intent
		Intent intent = getIntent();

		//From the intent, retrieve the drawable
		String imageString = intent.getStringExtra(MainActivity.INTENT_MESSAGE);
		Log.v("BRUNIUS_INTENT_RECEIVE", "Received intent with message " + imageString);

		//Decide which image the string correlates with
		Drawable finalDrawable;
		String finalString;
		switch (imageString) {
			case "crepes":
				finalDrawable = ContextCompat.getDrawable(getApplicationContext(), R.drawable.crepes);
				finalString = getString(R.string.crepes);
				break;
			case "plepie":
				finalDrawable = ContextCompat.getDrawable(getApplicationContext(), R.drawable.apple_pie);
				finalString = getString(R.string.apple_pie);
				break;
			case "oneggs":
				finalDrawable = ContextCompat.getDrawable(getApplicationContext(), R.drawable.bacon_eggs);
				finalString = getString(R.string.bacon_eggs);
				break;
			case "sorbet":
				finalDrawable = ContextCompat.getDrawable(getApplicationContext(), R.drawable.sorbet);
				finalString = getString(R.string.sorbet);
				break;
			default:
				finalDrawable = ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_error_black_24dp);
				finalString = getString(R.string.error);
				Log.v("BRUNIUS_INTENT_RECEIVE", "No drawable-compatible string located for " + imageString + " - displaying error image");
		}

		//Set the large image to be the drawable
		ImageView largeImage = (ImageView) findViewById(R.id.large_image);
		largeImage.setImageDrawable(finalDrawable);

		TextView largeText = (TextView) findViewById(R.id.image_description);
		largeText.setText(finalString);
	}

}
