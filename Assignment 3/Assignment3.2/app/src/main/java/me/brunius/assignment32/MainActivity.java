package me.brunius.assignment32;

import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;

public class MainActivity extends AppCompatActivity {
	public static final String INTENT_MESSAGE = "me.brunius.assignment32.IMAGE";
	public static final String LOG_PREFIX = "BRUNIUS_";


	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Load pictures into buttons
		ImageButton button_crepes = (ImageButton) findViewById(R.id.button_crepes);
		button_crepes.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.crepes));

		ImageButton button_baconeggs = (ImageButton) findViewById(R.id.button_baconeggs);
		button_baconeggs.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.bacon_eggs));

		ImageButton button_applepie = (ImageButton) findViewById(R.id.button_applepie);
		button_applepie.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.apple_pie));

		ImageButton button_sorbet = (ImageButton) findViewById(R.id.button_sorbet);
		button_sorbet.setImageDrawable(ContextCompat.getDrawable(getApplicationContext(), R.drawable.sorbet));

		Log.v(LOG_PREFIX + "ONCREATE", "Finished assigning images.");
    }

    public void goToBigImage(View view) {
		Intent imageIntent = new Intent(this, imageActivity.class);
		String tempString = view.toString().substring(view.toString().length()-7, view.toString().length()-1);
		imageIntent.putExtra(INTENT_MESSAGE, tempString);
		Log.v(LOG_PREFIX + "ONCLICK", "Sending intent now, with message " + imageIntent.getStringExtra(MainActivity.INTENT_MESSAGE));
		startActivity(imageIntent);
	}
}
