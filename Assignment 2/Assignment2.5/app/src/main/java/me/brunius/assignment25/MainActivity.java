package me.brunius.assignment25;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //These aren't going to change after creation, so they're found here instead of within the onClick event
        final EditText miles_object = (EditText) findViewById(R.id.edit_miles);
        final EditText feet_object = (EditText) findViewById(R.id.edit_feet);
        final EditText inches_object = (EditText) findViewById(R.id.edit_inches);
        final TextView text_converted = (TextView) findViewById(R.id.text_converted);
        final CheckBox check_metres = (CheckBox) findViewById(R.id.check_metres);

        final Button button = (Button) findViewById(R.id.convert_button);

        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                /* When the user presses the button, it should:
                 * a) Sum up the number of inches in total (A mile is 63360 inches, a foot is 12 inches, add them all up)
                 * b) Convert that number of inches to centimetres
                 * c) Decide whether we should be using centimetres or metres
                 * d) Put numbers and stuff into variable
                 * e) Write variable to view
                 */

                String debug_tag = "BRUNIUS_ONCLICK_DEBUG";

                //Find the number of miles
                Float miles;

                //If we try to call the parseFloat on an empty string, it crashes
                if (miles_object.getText().toString().trim().isEmpty()) {
                    miles = 0f;
                } else {
                    miles = Float.parseFloat(miles_object.getText().toString());
                }
                Log.v(debug_tag, "Miles: " + ((Float) miles).toString());

                //Find the number of feets
                Float feet;

                //If we try to call the parseFloat on an empty string, it crashes
                if (feet_object.getText().toString().trim().isEmpty()) {
                    feet = 0f;
                } else {
                    feet = Float.parseFloat(feet_object.getText().toString());
                }
                Log.v(debug_tag, "Feet: " + ((Float) feet).toString());

                //Find the number of inches
                Float inches;

                //If we try to call the parseFloat on an empty string, it crashes
                if (inches_object.getText().toString().trim().isEmpty()) {
                    inches = 0f;
                } else {
                    inches = Float.parseFloat(inches_object.getText().toString());
                }
                Log.v(debug_tag, "Inches: " + ((Float) inches).toString());

                //Find total inches. Inches + Feet*12 + Miles*63360
                //Normally I would try to avoid such 'magic numbers', but I doubt the number of inches in a foot is going to change...
                Float total_inches = inches + feet * 12f + miles * 63360f;
                Log.v(debug_tag, "Total inches: " + ((Float) total_inches).toString());

                //One inch is 2.54cm
                Float centimetres = total_inches * 2.54f;
                Log.v(debug_tag, "Centimetres: " + centimetres.toString());

                //Metres conversion via checkbox
                String text_output;
                if (check_metres.isChecked()) {
                    Float metres = centimetres / 100f;
                    text_output = String.format("%.2f m", metres);
                } else {
                    text_output = String.format("%.2f cm", centimetres);
                }
                Log.v(debug_tag, "Text output: " + text_output);

                //Write final text to screen
                text_converted.setText(text_output);
            }
        });
    }
}
